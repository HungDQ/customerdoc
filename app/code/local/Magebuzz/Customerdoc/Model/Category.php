<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Customerdoc_Model_Category extends Mage_Core_Model_Abstract {

  public function _construct() {
    parent::_construct();
    $this->_init('customerdoc/category');
  }
}