<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Customerdoc_Model_Mysql4_Category extends Mage_Core_Model_Mysql4_Abstract {
  public function _construct() {
    $this->_init('customerdoc/category', 'id');
  }
}