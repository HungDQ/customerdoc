<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Customerdoc_Model_Mysql4_Category_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {
  public function _construct() {
    parent::_construct();
    $this->_init('customerdoc/category');
  }

  public function toOptionArray()
  {
    return $this->_toOptionArray();
  }

  protected function _toOptionArray($valueField='id', $labelField='name', $additional=array())
  {
    $res = array();
    $additional['value'] = $valueField;
    $additional['label'] = $labelField;

    foreach ($this as $item) {
      foreach ($additional as $code => $field) {
        $data[$code] = $item->getData($field);
      }
      $res[] = $data;
    }
    $defaultValue = array('value'=>'','label'=>'Please select a category');
    array_unshift($res, $defaultValue);
    return $res;
  }
}