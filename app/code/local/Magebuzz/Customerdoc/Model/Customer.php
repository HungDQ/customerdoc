<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Customerdoc_Model_Customer extends Mage_Core_Model_Abstract {
  public function _construct() {
    parent::_construct();
    $this->_init('customerdoc/customer');
  }

  public function saveDocumentCustomer($newCustomerIds, $docId)
  {
    $oldDocumentCustomerModel = $this->getCollection()->addFieldToFilter('document_id',$docId);
    $oldCustomerIds = $oldDocumentCustomerModel->getColumnValues('customer_id');
    if(!$newCustomerIds){
      $deleteCustomer = Mage::getModel('customerdoc/customer')->getCollection()->addFieldToFilter('document_id', $docId);
      if (count($deleteCustomer->getData()) > 0) {
        foreach ($deleteCustomer as $del) {
          $del->delete();
        }
      }
      return;
    }
    $insert = array_diff($newCustomerIds, $oldCustomerIds);
    $delete = array_diff($oldCustomerIds, $newCustomerIds);

    if ($delete) {
      $deleteCustomer = Mage::getModel('customerdoc/customer')->getCollection()->addFieldToFilter('customer_id', array('in' => $delete))
        ->addFieldToFilter('document_id', $docId);
      if (count($deleteCustomer->getData()) > 0) {
        foreach ($deleteCustomer as $del) {
          $del->delete();
        }
      }
    }
    if ($insert) {
      foreach ($insert as $customerId) {
        $customerSelected = Mage::getModel('customerdoc/customer');
        $customerSelected->setCustomerId($customerId);
        $customerSelected->setDocumentId($docId);
        $customerSelected->save();
        if(Mage::helper('customerdoc')->canSendEmail($docId)){
          $customerSelected->sendEmailToNewCustomer($customerId, $docId);
        }
      }
    }
  }

  public function sendEmailToNewCustomer($customerId, $docId)
  {
    $customer = Mage::getModel('customer/customer')->load($customerId);
    $customerStoreId = $customer->getStoreId();
    $document = Mage::getModel('customerdoc/document')->load($docId);
    if($document->getDocumentLink()){
      $link = 'Download link: '.Mage::helper("adminhtml")->getUrl('customerdoc/adminhtml_document/download', array('id' => $docId));
    }else{
      $link = '';
    }
    $mailTemplate = Mage::getModel('core/email_template');
    /* @var $mailTemplate Mage_Core_Model_Email_Template */
    $mailTemplate->sendTransactional(
      Mage::getStoreConfig('customerdoc/document_options/email_template', $customerStoreId),
      Mage::getStoreConfig('contacts/email/sender_email_identity', $customerStoreId),
      $customer->getEmail(),
      null,
      array(
        'customername' => $customer->getFirstname(),
        'name' => $document->getName(),
        'description' => $document->getDescription(),
        'link' => $link
      )
    );
  }
}