<?php
class Magebuzz_Customerdoc_Helper_Data extends Mage_Core_Helper_Abstract
{
  public function renameFile($fileName)
  {
    $string = str_replace("  ", " ", $fileName);
    $newFileName = str_replace(" ", "-", $string);
    $newFileName = strtolower($newFileName);
    return $newFileName;
  }

  public function loadCategoryNameById($id)
  {
    $status = Mage::getModel('customerdoc/category')->load($id)->getStatus();
    if($status !== '1'){
      return 'Uncategorized';
    }
    return Mage::getModel('customerdoc/category')->load($id)->getName();
  }

  public function loadDocCollectionByCatId($catId, $customerId, $groupId)
  {
    $documentIds = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('status',1)
        ->addFieldToFilter('category',$catId)->getAllIds();

    $documentGroupIds = Mage::getModel('customerdoc/customergroup')->getCollection()->addFieldToFilter('customergroup_id',$groupId)->getColumnValues('document_id');
    $documentCustomerIds = Mage::getModel('customerdoc/customer')->getCollection()->addFieldToFilter('customer_id',$customerId)->getColumnValues('document_id');
    $result = array_unique(array_merge($documentCustomerIds, $documentGroupIds));
    $finalDocIds = array_intersect($result, $documentIds);
    $documents = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('id', array('in' => $finalDocIds));
    return $documents;
  }

  public function canSendEmail($docId)
  {
    $sendEmail = Mage::getModel('customerdoc/document')->load($docId)->getSendEmail();
    if($sendEmail == 1){
      return true;
    }else{
      return false;
    }
  }

  public function isCategoryEnabled($catId)
  {
    return Mage::getModel('customerdoc/category')->load($catId)->getStatus();
  }
}
