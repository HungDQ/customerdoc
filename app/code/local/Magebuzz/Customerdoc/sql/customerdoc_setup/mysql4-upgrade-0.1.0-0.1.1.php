<?php
$installer = $this;
$installer->startSetup();

$installer->run("
  ALTER TABLE `{$this->getTable('document_category')}` ADD COLUMN `status` smallint(6) NOT NULL default '1';
");
$installer->endSetup();