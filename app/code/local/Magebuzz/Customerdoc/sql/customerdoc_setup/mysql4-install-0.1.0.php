<?php
$installer = $this;
$installer->startSetup();

$installer->run("
 DROP TABLE IF EXISTS {$this->getTable('document')};
  CREATE TABLE {$this->getTable('document')} (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` text NOT NULL default '',
  `description` text NOT NULL default '',
  `category` smallint(5) NOT NULL default '0',
  `send_email` smallint(6) NOT NULL default '2',
  `status` smallint(6) NOT NULL default '1',
  `document_link` text NOT NULL default '',
  `thumbnail` varchar(255) NOT NULL default '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 DROP TABLE IF EXISTS {$this->getTable('document_customer')};
  CREATE TABLE {$this->getTable('document_customer')} (
  `id` int(10) unsigned NOT NULL auto_increment,
  `document_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `CUSTOMER_DOC_ID` (`document_id`),
  CONSTRAINT `CUSTOMER_DOC_ID` FOREIGN KEY (`document_id`) REFERENCES `{$this->getTable('document')}`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 DROP TABLE IF EXISTS {$this->getTable('document_customer_group')};
  CREATE TABLE {$this->getTable('document_customer_group')} (
  `id` int(10) unsigned NOT NULL auto_increment,
  `document_id` int(10) unsigned NOT NULL,
  `customergroup_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `CUSTOMERGROUP_DOC_ID` (`document_id`),
  CONSTRAINT `CUSTOMERGROUP_DOC_ID` FOREIGN KEY (`document_id`) REFERENCES `{$this->getTable('document')}`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('document_category')};
  CREATE TABLE {$this->getTable('document_category')} (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` text NOT NULL default '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();
