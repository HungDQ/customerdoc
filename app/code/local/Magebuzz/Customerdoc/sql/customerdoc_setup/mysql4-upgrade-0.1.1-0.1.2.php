<?php
$installer = $this;
$installer->startSetup();

$installer->run("
  ALTER TABLE `{$this->getTable('document')}` ADD COLUMN `created_time` TIMESTAMP NULL;
");
$installer->endSetup();