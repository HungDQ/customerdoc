<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Customerdoc_Block_Adminhtml_Document extends Mage_Adminhtml_Block_Widget_Grid_Container {
  public function __construct() {
    $this->_controller = 'adminhtml_document';
    $this->_blockGroup = 'customerdoc';
    $this->_headerText = Mage::helper('customerdoc')->__('Manage Document');
    $this->_addButtonLabel = Mage::helper('customerdoc')->__('Add Document');
    parent::__construct();
  }
}