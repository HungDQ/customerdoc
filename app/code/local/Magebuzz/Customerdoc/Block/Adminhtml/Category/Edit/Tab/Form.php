<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Customerdoc_Block_Adminhtml_Category_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

  protected function _prepareForm() {
    $form = new Varien_Data_Form();
    $this->setForm($form);
    $fieldset = $form->addFieldset('customerdoc_form', array('legend' => Mage::helper('customerdoc')->__('Category information')));

    if(Mage::registry('customerdoc_category')) {
      $data = Mage::registry('customerdoc_category')->getData();
    }
  
    $fieldset->addField('name', 'text', array(
      'label'     => Mage::helper('customerdoc')->__('Category Name'),
      'class'     => 'required-entry',
      'required'  => true,
      'name'      => 'name',
    ));

    $fieldset->addField('status', 'select', array(
        'label' => Mage::helper('customerdoc')->__('Status'),
        'name' => 'status',
        'values' => array(
          array('value' => 1, 'label' => Mage::helper('customerdoc')->__('Enabled'),),
          array('value' => 2, 'label' => Mage::helper('customerdoc')->__('Disabled'),)
        ,)
      ,)
    );

    $form->setValues($data);
    return parent::_prepareForm();
  }
}