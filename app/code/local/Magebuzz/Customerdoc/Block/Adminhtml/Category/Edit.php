<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Customerdoc_Block_Adminhtml_Category_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {
  public function __construct() {
    parent::__construct();
    $this->_objectId = 'id';
    $this->_blockGroup = 'customerdoc';
    $this->_controller = 'adminhtml_category';

    $this->_updateButton('save', 'label', Mage::helper('customerdoc')->__('Save Category'));
    $this->_updateButton('delete', 'label', Mage::helper('customerdoc')->__('Delete Category'));

    $this->_addButton('saveandcontinue', array('label' => Mage::helper('adminhtml')->__('Save And Continue Edit'), 'onclick' => 'saveAndContinueEdit()', 'class' => 'save',), -100);

    $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('customerdoc_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'customerdoc_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'customerdoc_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
  }

  protected function _prepareLayout()
  {
    $return = parent::_prepareLayout();
    if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
      $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
      $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
    }
    return $return;
  }

  public function getHeaderText() {
    if (Mage::registry('customerdoc_category') && Mage::registry('customerdoc_category')->getId()) {
      return Mage::helper('customerdoc')->__("Edit '%s'", $this->htmlEscape(Mage::registry('customerdoc_category')->getName()));
    } else {
      return Mage::helper('customerdoc')->__('Add New Category');
    }
  }
}