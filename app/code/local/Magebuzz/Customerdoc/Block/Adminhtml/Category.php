<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Customerdoc_Block_Adminhtml_Category extends Mage_Adminhtml_Block_Widget_Grid_Container {
  public function __construct() {
    $this->_controller = 'adminhtml_category';
    $this->_blockGroup = 'customerdoc';
    $this->_headerText = Mage::helper('customerdoc')->__('Manage Category');
    $this->_addButtonLabel = Mage::helper('customerdoc')->__('Add Category');
    parent::__construct();
  }
}