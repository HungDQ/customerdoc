<?php
class Magebuzz_Customerdoc_Block_Document extends Mage_Core_Block_Template
{
  public function __construct()
  {
    parent::__construct();
    $this->setTemplate('customerdoc/document.phtml');
    $customer = Mage::getSingleton('customer/session')->getCustomer();
    $customerId = $customer->getId();
    $groupId = $customer->getGroupId();
    $documentCustomerIds = Mage::getModel('customerdoc/customer')->getCollection()->addFieldToFilter('customer_id',$customerId)
      ->getColumnValues('document_id');

    $documentGroupIds = Mage::getModel('customerdoc/customergroup')->getCollection()->addFieldToFilter('customergroup_id',$groupId)
      ->getColumnValues('document_id');
    $documentIds = array_merge($documentCustomerIds, $documentGroupIds);
    $documents = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('status',1)
      ->addFieldToFilter('id',array('in' => $documentIds));
    if($params = $this->getRequest()->getPost()){
      Mage::getSingleton('core/session')->setFilterRequest($params);
      foreach($params as $key=>$param){
        if(($key == 'description' || $key == 'name') && $param!=''){
          $documents->addFieldToFilter($key,array('like'=>'%'.$param.'%'));
        }
        elseif($key == 'category' && $param !== ''){
          if($param == '0'){
            Mage::getSingleton('core/session')->setEmptyFilter(true);
          }else{
            $documents->addFieldToFilter('category', $param);
          }
        }
      }
      $from = $params['from'];
      $timeFrom = date('Y/m/d H:i:s',strtotime($from));
      $to = $params['to'];
      $timeTo = date('Y/m/d H:i:s', strtotime($to));
      if(($from !== '') && ($to !== '')){
        $documents->addFieldToFilter('created_time',array(
          'from' => $timeFrom,
          'to'   => $timeTo
        ));
      }

      if(count($documents) < 1){
        Mage::getSingleton('core/session')->setEmptyFilter(true);
      }
    }
    if($data = $this->getRequest()->getParam('category')){
      $documents->setOrder('category', $data);
    }
    $categoryIds = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('id', array('in' => $documentIds))
      ->getColumnValues('category');

    if(is_array($categoryIds)){
      $categoryIds = array_unique($categoryIds);
      foreach($categoryIds as $key => $catId){
        if(Mage::helper('customerdoc')->isCategoryEnabled($catId) !== '1'){
          $categoryIds[$key] = '0';
        }
      }
      $this->setCategoryIds(array_unique($categoryIds));
    }

    $this->setDocuments($documents);

  }

  protected function _prepareLayout()
  {
    parent::_prepareLayout();
    $customer = Mage::getSingleton('customer/session')->getCustomer();
    $customerId = $customer->getId();
    $groupId = $customer->getGroupId();
    $pager = $this->getLayout()->createBlock('page/html_pager', 'documents.pager')
      ->setCollection($this->getDocuments());
    $this->setChild('pager', $pager);
    $this->getDocuments()->load();

    $categoryIds = $this->getCategoryIds();
    if(count($categoryIds) > 0){
      foreach($categoryIds as $categoryId){
        $catDocuments = Mage::helper('customerdoc')->loadDocCollectionByCatId($categoryId, $customerId, $groupId);
        $catPager = $this->getLayout()->createBlock('page/html_pager', 'documents.pager')
          ->setCollection($catDocuments);
        $childName = 'pager'.$categoryId;
        $this->setChild($childName, $catPager);
        $catDocuments->load();
      }
    }

    return $this;
  }

  public function getPagerHtml()
  {
    return $this->getChildHtml('pager');
  }

  public function getCategoryPager($catId)
  {
    $childName = 'pager'.$catId;
    return $this->getChildHtml($childName);
  }
}