<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Customerdoc_Adminhtml_DocumentController extends Mage_Adminhtml_Controller_Action
{
  protected function _initAction()
  {
    $this->loadLayout()->_setActiveMenu('customerdoc/items')->_addBreadcrumb(Mage::helper('adminhtml')->__('Document Document'), Mage::helper('adminhtml')->__('Document Document'));

    return $this;
  }

  public function indexAction()
  {
    $this->_initAction()->renderLayout();
  }

  public function editAction()
  {
    $id = $this->getRequest()->getParam('id');
    $model = Mage::getModel('customerdoc/document');

    if ($id) {
      $model->load($id);
      $data = Mage::getSingleton('adminhtml/session')->getFormData(TRUE);
      if (!empty($data)) {
        $model->setData($data);
      }
    }
    Mage::register('customerdoc_document', $model);
    $this->loadLayout();
    $this->_setActiveMenu('customerdoc/items');

    $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
    $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

    $this->getLayout()->getBlock('head')->setCanLoadExtJs(TRUE);

    $this->_addContent($this->getLayout()->createBlock('customerdoc/adminhtml_document_edit'))->_addLeft($this->getLayout()->createBlock('customerdoc/adminhtml_document_edit_tabs'));

    $this->renderLayout();
  }

  public function newAction()
  {
    $this->_redirect('*/*/edit');
  }

  public function saveAction()
  {
    if ($data = $this->getRequest()->getPost()) {
      $model = Mage::getModel('customerdoc/document');
      $id = $this->getRequest()->getParam('id');
      $todayDate = strtotime(date("d-m-Y H:i:s", Mage::getModel('core/date')->timestamp(time())));
      $postDate = strtotime($data['created_time']);
      if($postDate > $todayDate){
        Mage::getSingleton('core/session')->addError('Invalid Date');
        $this->getResponse()->setRedirect($this->getUrl('*/adminhtml_document/edit', array('id' => $id)));
        return;
      }
      if(isset($_FILES['document_link']['name']) and (file_exists($_FILES['document_link']['tmp_name']))) {
        try {
          $uploader = new Varien_File_Uploader('document_link');
          $fileName = $_FILES['document_link']['name'];
          $newFileName = Mage::helper('customerdoc')->renameFile($fileName);
          if(Mage::getStoreConfig('customerdoc/document_options/restricted_type') !== ''){
            $allowedTypes = explode(',',Mage::getStoreConfig('customerdoc/document_options/restricted_type'));
            $uploader->setAllowedExtensions($allowedTypes);
          }else{
            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
          }

          $uploader->setAllowRenameFiles(false);
          $uploader->setFilesDispersion(false);

          $path = Mage::getBaseDir('media') . DS . '/document' . DS ;
          if (!is_dir($path)) {
            mkdir($path, 0777, TRUE);
          }
          if (!file_exists($path . DS . $newFileName)) {
            $uploader->save($path, $newFileName);
          }

          $data['document_link'] = $newFileName;
        }catch(Exception $e) {
          Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
          $this->getResponse()->setRedirect($this->getUrl('*/adminhtml_document/edit', array('id' => $id)));
          return;
        }
      }elseif ($id && !isset($data['document_link']['delete'])) {
        if($model->load($id)->getDocumentLink()){
          $data['document_link'] = $model->getDocumentLink();
        }
      }

      if (isset($data['document_link']['delete']) && $data['document_link']['delete'] == 1) {
        $data['document_link'] = '';
      }

      if (isset($_FILES['thumbnail']['name']) && $_FILES['thumbnail']['name'] != '') {
        try {
          $imageName = $_FILES['thumbnail']['name'];
          $newImageName = Mage::helper('customerdoc')->renameFile($imageName);
          $uploader = new Varien_File_Uploader('thumbnail');
          $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
          $uploader->setAllowRenameFiles(TRUE);
          $uploader->setFilesDispersion(FALSE);

          $path = Mage::getBaseDir('media') . DS . 'document' . DS . 'thumbnail';
          if (!is_dir($path)) {
            mkdir($path, 0777, TRUE);
          }
          if (!file_exists($path . DS . $newImageName)) {
            $uploader->save($path, $newImageName);
          }
        } catch (Exception $e) {
          Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $data['thumbnail'] = $newImageName;
      } elseif ($id && !isset($data['thumbnail']['delete'])) {
        if($model->load($id)->getThumbnail()){
          $data['thumbnail'] = $model->getThumbnail();
        }
      }

      if (isset($data['thumbnail']['delete']) && $data['thumbnail']['delete'] == 1) {
        $data['thumbnail'] = '';
      }
      if ($id) {
        $model->load($id);
        $model->setData($data)->setId($this->getRequest()->getParam('id'));
      }else{
        $model->setData($data);
      }

      try {
        $model->save();

        $cusGroupIds = $this->getRequest()->getParam('customergroup_id');
        Mage::getModel('customerdoc/customergroup')->saveDocumentCusGroup($cusGroupIds, $model->getId());

        $customerIds = $this->getRequest()->getParam('selected_customer');
        if(isset($customerIds)){
          Mage::getModel('customerdoc/customer')->saveDocumentCustomer($customerIds, $model->getId());
        }

        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully saved'));
        Mage::getSingleton('adminhtml/session')->setFormData(FALSE);

        if ($this->getRequest()->getParam('back')) {
          $this->_redirect('*/*/edit', array('id' => $model->getId()));
          return;
        }

        $this->_redirect('*/*/');
        return;
      } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
        return;
      }
    }
    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('customerdoc')->__('Unable to find item to save'));
    $this->_redirect('*/*/');
  }

  public function deleteAction()
  {
    if ($this->getRequest()->getParam('id') > 0) {
      try {
        $model = Mage::getModel('customerdoc/document');

        $model->setId($this->getRequest()->getParam('id'))->delete();

        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
        $this->_redirect('*/*/');
      } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
      }
    }
    $this->_redirect('*/*/');
  }

  public function massDeleteAction()
  {
    $document = $this->getRequest()->getParam('document');
    if (!is_array($document)) {
      Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
    } else {
      try {
        foreach ($document as $province) {
          $provinceModel = Mage::getModel('customerdoc/document')->load($province);
          $provinceModel->delete();
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($document)));
      } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
      }
    }
    $this->_redirect('*/*/index');
  }

  public function massStatusAction()
  {
    $documentIds = $this->getRequest()->getParam('document');
    if (!is_array($documentIds)) {
      Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
    } else {
      try {
        foreach ($documentIds as $documentId) {
          Mage::getSingleton('customerdoc/document')->load($documentId)->setStatus($this->getRequest()->getParam('status'))->setIsMassupdate(TRUE)->save();
        }
        $this->_getSession()->addSuccess($this->__('Total of %d record(s) were successfully updated', count($documentIds)));
      } catch (Exception $e) {
        $this->_getSession()->addError($e->getMessage());
      }
    }
    $this->_redirect('*/*/index');
  }


  public function dcustomerAction()
  {
    $this->loadLayout();
    $this->getLayout()->getBlock('document.edit.tab.customer')->setDocument($this->getRequest()->getPost('odocument', null));
    $this->renderLayout();
  }

  public function dcustomerGridAction()
  {
    $this->loadLayout();
    $this->getLayout()->getBlock('document.edit.tab.customer')->setDocument($this->getRequest()->getPost('odocument', null));
    $this->renderLayout();
  }

  public function downloadAction() {
    $docId = $this->getRequest()->getParam('id');
    $filename = Mage::getModel('customerdoc/document')->load($docId)->getDocumentLink();
    $filepath = Mage::getBaseDir('media').DS.'document'.DS.$filename;
    if (! is_file ( $filepath ) || ! is_readable ( $filepath )) {
      throw new Exception ( );
    }
    $this->getResponse ()
      ->setHttpResponseCode ( 200 )
      ->setHeader ( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true )
      ->setHeader ( 'Pragma', 'public', true )
      ->setHeader ( 'Content-type', 'application/force-download' )
      ->setHeader ( 'Content-Length', filesize($filepath) )
      ->setHeader ('Content-Disposition', 'attachment' . '; filename=' . basename($filepath) );
    $this->getResponse ()->clearBody ();
    $this->getResponse ()->sendHeaders ();
    readfile ( $filepath );
    exit;
  }
}