<?php

/*
* Copyright (c) 2014 www.magebuzz.com
*/

class Magebuzz_Customerdoc_Adminhtml_CategoryController extends Mage_Adminhtml_Controller_Action
{
  protected function _initAction()
  {
    $this->loadLayout()->_setActiveMenu('customerdoc/items')->_addBreadcrumb(Mage::helper('adminhtml')->__('Document Category'), Mage::helper('adminhtml')->__('Document Category'));

    return $this;
  }

  public function indexAction()
  {
    $this->_initAction()->renderLayout();
  }

  public function editAction()
  {
    $id = $this->getRequest()->getParam('id');
    $model = Mage::getModel('customerdoc/category');

    if ($id) {
      $model->load($id);
      $data = Mage::getSingleton('adminhtml/session')->getFormData(TRUE);
      if (!empty($data)) {
        $model->setData($data);
      }
    }
    Mage::register('customerdoc_category', $model);
    $this->loadLayout();
    $this->_setActiveMenu('customerdoc/items');

    $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
    $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

    $this->getLayout()->getBlock('head')->setCanLoadExtJs(TRUE);

    $this->_addContent($this->getLayout()->createBlock('customerdoc/adminhtml_category_edit'))->_addLeft($this->getLayout()->createBlock('customerdoc/adminhtml_category_edit_tabs'));

    $this->renderLayout();
  }

  public function newAction()
  {
    $this->_redirect('*/*/edit');
  }

  public function saveAction()
  {
    if ($data = $this->getRequest()->getPost()) {
      $model = Mage::getModel('customerdoc/category');
      if ($id = $this->getRequest()->getParam('id')) {
        $model->load($id);
        $model->setData($data)->setId($this->getRequest()->getParam('id'));
      }else{
        $model->setData($data);
      }

      try {
        $model->save();

        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully saved'));
        Mage::getSingleton('adminhtml/session')->setFormData(FALSE);

        if ($this->getRequest()->getParam('back')) {
          $this->_redirect('*/*/edit', array('id' => $model->getId()));
          return;
        }

        $this->_redirect('*/*/');
        return;
      } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        Mage::getSingleton('adminhtml/session')->setFormData($data);
        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
        return;
      }
    }
    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('customerdoc')->__('Unable to find item to save'));
    $this->_redirect('*/*/');
  }

  public function deleteAction()
  {
    if ($this->getRequest()->getParam('id') > 0) {
      try {
        $model = Mage::getModel('customerdoc/category');

        $model->setId($this->getRequest()->getParam('id'))->delete();

        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
        $this->_redirect('*/*/');
      } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
      }
    }
    $this->_redirect('*/*/');
  }

  public function massDeleteAction()
  {
    $category = $this->getRequest()->getParam('category');
    if (!is_array($category)) {
      Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
    } else {
      try {
        foreach ($category as $province) {
          $provinceModel = Mage::getModel('customerdoc/category')->load($province);
          $provinceModel->delete();
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($category)));
      } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
      }
    }
    $this->_redirect('*/*/index');
  }

  public function massStatusAction()
  {
    $categoryIds = $this->getRequest()->getParam('category');
    if (!is_array($categoryIds)) {
      Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
    } else {
      try {
        foreach ($categoryIds as $categoryId) {
          Mage::getSingleton('customerdoc/category')->load($categoryId)->setStatus($this->getRequest()->getParam('status'))->setIsMassupdate(TRUE)->save();
        }
        $this->_getSession()->addSuccess($this->__('Total of %d record(s) were successfully updated', count($categoryIds)));
      } catch (Exception $e) {
        $this->_getSession()->addError($e->getMessage());
      }
    }
    $this->_redirect('*/*/index');
  }
}