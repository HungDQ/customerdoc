<?php
class Magebuzz_Customerdoc_IndexController extends Mage_Core_Controller_Front_Action
{
  public function indexAction()
  {
    $session = Mage::getSingleton( 'customer/session' );
    $isLoggedIn = $session->isLoggedIn();
    if(!$isLoggedIn){
      $session->addNotice('Please Log in to see your documents');
      $session->setBeforeAuthUrl(Mage::getUrl('customerdoc'));
      $this->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
    }
    $this->loadLayout();
    $this->getLayout()->getBlock('head')->setTitle($this->__('Document'));
    $this->renderLayout();
  }

  public function downloadAction() {
    $isLoggedIn = Mage::getSingleton('customer/session')->isLoggedIn();
    if(!$isLoggedIn){
      $this->getResponse()->setRedirect(Mage::getBaseUrl());
    }
    $docId = $this->getRequest()->getParam('id');
    $filename = Mage::getModel('customerdoc/document')->load($docId)->getDocumentLink();
    $filepath = Mage::getBaseDir('media').DS.'document'.DS.$filename;
    if (! is_file ( $filepath ) || ! is_readable ( $filepath )) {
      Mage::getSingleton('core/session')->addError('Cannot download this file');
      $this->_redirect('customerdoc/index/index');
    }
    $this->getResponse ()
      ->setHttpResponseCode ( 200 )
      ->setHeader ( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true )
      ->setHeader ( 'Pragma', 'public', true )
      ->setHeader ( 'Content-type', 'application/force-download' )
      ->setHeader ( 'Content-Length', filesize($filepath) )
      ->setHeader ('Content-Disposition', 'attachment' . '; filename=' . basename($filepath) );
    $this->getResponse ()->clearBody ();
    $this->getResponse ()->sendHeaders ();
    readfile ( $filepath );
    exit;
  }

  public function viewAction(){
    $isLoggedIn = Mage::getSingleton('customer/session')->isLoggedIn();
    if(!$isLoggedIn){
      $this->getResponse()->setRedirect(Mage::getBaseUrl());
    }
    $docId = $this->getRequest()->getParam('id');
    $docLink = Mage::getModel('customerdoc/document')->load($docId)->getDocumentLink();
    $url = 'media/document/'.$docLink;
    Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl().$url);
    return;
  }
}